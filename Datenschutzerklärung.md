# Datenschutzerklärung

Datenschutzerklärung für die Android-Application CatWalk-CatWander-CatRun

1. Datenschutz auf einen Blick

Allgemeine Hinweise

Die folgenden Hinweise geben einen einfachen Überblick darüber, was mit Ihren personenbezogenen Daten passiert, wenn Sie diese App nutzen. Personenbezogene Daten sind alle Daten, mit denen Sie persönlich identifiziert werden können. Ausführliche Informationen zum Thema Datenschutz entnehmen Sie unserer unter diesem Text aufgeführten Datenschutzerklärung.

Datenerfassung in der App

Wie werden die Daten erfasst?

Erfasst werden Daten über den Standort während der Nutzung.
Diese werden per GPS erfasst und nur auf dem eigenen Gerät gespeichert und verarbeit.
Zu keiner Zeit werden diese Daten an eine dritte Person gesendet.

Bei fragen erreicht man mich unter folgender e-mail adresse

kajusvoncarstein@gmail.com
